//PRD
var apiOMP = "https://ripleypuntos.com.pe/clientwebapi/api/clientomp/registerclientomp"; 
var apiPromotion ="https://ripleypuntos.com.pe/clientwebapi/api/promotion/registerpromotion"; 
var api ="https://ripleypuntos.com.pe/clientwebapi/api/segmentacion/getsetmentantion"; 
var urlIdentity = "https://ripleypuntos.com.pe/identity/connect/token";

//QA
// var apiOMP = "https://ripleyptosqa.bancoripley.com.pe/QA/clientweb/registerclientomp"; 
// var apiPromotion ="https://ripleyptosqa.bancoripley.com.pe/QA/clientweb/registerpromotion"; 
// var api ="https://ripleyptosqa.bancoripley.com.pe/QA/clientweb/getsetmentantion"; 
// var urlIdentity = "https://ripleyptosqa.bancoripley.com.pe/QA/identity";

// //DEV
// var apiOMP = "https://2kgxr1j9x0.execute-api.us-east-1.amazonaws.com/loyalty-dev/clientweb/registerclientomp";
// var apiPromotion ="https://2kgxr1j9x0.execute-api.us-east-1.amazonaws.com/loyalty-dev/clientweb/registerpromotion";
// var api ="https://2kgxr1j9x0.execute-api.us-east-1.amazonaws.com/loyalty-dev/clientweb/getsetmentantion"
// var urlIdentity = "https://2kgxr1j9x0.execute-api.us-east-1.amazonaws.com/loyalty-dev/identity";


function getAuthentication(){
    return $.ajax({
        type: "POST",
        async: false,
        url: urlIdentity,
        data: 'grant_type=client_credentials&client_id=LoyaltyApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf',
        headers: {
            "Content-Type" : 'application/x-www-form-urlencoded'
        },
        dataType: 'json'
    });
}

function getPointsClient(client){
    var result = getAuthentication();
    var token = result.responseJSON.access_token;
    var autorizacion = "Bearer " + token;
    return $.ajax({
        type: "POST",
        url: api,
        data: JSON.stringify(client), //"filter=TIPO_DOCUMENTO=" + client.typeDocument + ";DOCUMENTO=" + client.numberDocument+ ";FLG_CLIENTE=1",
        headers: {
            "Content-Type" : 'application/json',
            "Authorization" : autorizacion,
        },
    });
}

function postRegisterClientOMP(clientOMP){
    var result = getAuthentication();
    var token = result.responseJSON.access_token;
    var autorizacion = "Bearer " + token;
    return $.ajax({
        type: "POST",
        url: apiOMP,
        data: JSON.stringify(clientOMP), //"filter=TIPO_DOCUMENTO=" + client.typeDocument + ";DOCUMENTO=" + client.numberDocument+ ";FLG_CLIENTE=1",
        headers: {
            "Content-Type" : 'application/json',
            "Authorization" : autorizacion,
        },
    });
}

function postRegisterPromociones(dataPromocion){
    var result = getAuthentication();
    var token = result.responseJSON.access_token;
    var autorizacion = "Bearer " + token;
    return $.ajax({
        type: "POST",
        url: apiPromotion,
        data: JSON.stringify(dataPromocion),
        headers: {
            "Content-Type" : 'application/json',
            "Authorization" : autorizacion,
        },
    });
}