$(document).ready(function() {
  $("#seccionGoldBeneficio").css("margin-left", "100%");
  $("#markerGoldBeneficio").hide();
});

function complete() {}

function mostrarSilverBeneficio(event) {
  $("#markerGoldBeneficio").hide();
  $("#markerGreyBeneficio").show();
  $("#seccionGoldBeneficio").animate(
    {
      "margin-left": "100%"
    },
    500
  );

  $("#seccionSilverBeneficio").animate(
    {
      "margin-left": "0%"
    },
    500
  );
  setTimeout(function(){ document.getElementById("seccionGoldBeneficio").style.display="none"; }, 400);
}
function mostrarGoldBeneficio(event) {
  $("#markerGreyBeneficio").hide();
  $("#markerGoldBeneficio").show();
  document.getElementById("seccionGoldBeneficio").style.display="block";
  $("#seccionSilverBeneficio").animate(
    {
      "margin-left": "-100%"
    },
    500
  );
  $("#seccionGoldBeneficio").animate(
    {
      "margin-left": "0%"
    },
    500
  );
}
function OpenPreguntasFrec(params) {
  let param1=params.split("-")[0];
  let param2=params.split("-")[1];
  for (let index = 1; index < 10; index++) {
    if(document.getElementById(param1+"-"+index) != null){
      if(param2==index){
        if(document.getElementById(params).style.display == "none"){
          document.getElementById(params).style.display="block";
        }
        else{
          document.getElementById(params).style.display="none";
        }    
      }
      else{
        document.getElementById(param1+"-"+index).style.display="none";
      }
    }
  }
  
  $('html, body').animate({
    scrollTop: $("#btn-"+params).offset().top - 75
    }, 900);
  
}