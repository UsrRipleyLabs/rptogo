let rutaImgE = 'img/canjes_promociones/experiencias/';
let rutaImgS = 'img/canjes_promociones/supercanjes/';
let rutaImgSVG = 'img/svg/';
let imgLogoWhite = 'rp-white-logo-white.svg';
let imgLogoWhite_min = 'rp-white-logo-white-min.svg';

  let items_experiencias = [
    // {
    //     "imgBack": "imgExperienciaNormal6.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "5,000 RPGo",
    //     "titlebottom" : "MoviePack",
    //     "subtitlebottom" : "3 entradas 2D + 2 gaseosas medianas + cancha gigante recargable. Hasta el 15 de Marzo"
    // },
    {
        "imgBack": "imgExperienciaNormal4.png",
        "pricecurrent": "4,500",
        "priceante": "5,000 RPGo",
        "titlebottom" : "Vale de S/ 50",
        "subtitlebottom" : "* Válido del 01 al 30 de abril.  Stock 50 vales."
    },
    {
        "imgBack": "imgExperienciaNormal5.png",
        "pricecurrent": "4,500",
        "priceante": "5,000 RPGo",
        "titlebottom" : "Gift card de S/ 50",
        "subtitlebottom" : "* Válido del 01 al 30 de abril.  Stock 50 gift cards."
    },
    {
        "imgBack": "imgExperienciaNormal3.png",
        "pricecurrent": "4,500",
        "priceante": "5,000 RPGo",
        "titlebottom" : "Vale de S/ 40",
        "subtitlebottom" : "* Válido del 01 al 30 de abril.  Stock 50 vales."
    },
    {
        "imgBack": "imgExperienciaNormal2.png",
        "pricecurrent": "4,500",
        "priceante": "5,000 RPGo",
        "titlebottom" : "Vale de S/ 50",
        "subtitlebottom" : "* Válido del 01 al 30 de abril.  Stock 50 vales."
    },
    {
        "imgBack": "imgExperienciaNormal6.png",
        "pricecurrent": "4,500",
        "priceante": "6,000 RPGo",
        "titlebottom" : "2 entradas 2D + cancha gigante recargable",
        "subtitlebottom" : "* Válido del 01 al 30 de abril. Stock 50 vales"
    }
    // {
    //     "imgBack": "imgExperienciaNormal6.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal1.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal2.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal3.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal4.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal5.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal6.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal1.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // },
    // {
    //     "imgBack": "imgExperienciaNormal2.png",
    //     "pricecurrent": "2,500",
    //     "priceante": "22,900 RPGo",
    //     "titlebottom" : "LOL Surprise",
    //     "subtitlebottom" : "Bubly Surprise"
    // }
  ];

  let items_supercanjes = [
    // {
    //     "imgBack": "Versace-Crystal.png",
    //     "pricecurrent": "4,000",
    //     "priceother":"+ S/ 25",
    //     "priceante": "9,900 RPGo",
    //     "titlebottom" : "Versace",
    //     "subtitlebottom" : "Crystal Noir EDT 30 ml – SKU 23754594"
    // },
    {
        "imgBack": "azzaro-mademoiselle.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "9,900 RPGo",
        "titlebottom" : "Azzaro",
        "subtitlebottom" : "Mademoiselle Azzaro EDT 30 ml – SKU 21624858"
    },
    {
        "imgBack": "lancome-perfume-miracle.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "8,900 RPGo",
        "titlebottom" : "Lancome",
        "subtitlebottom" : "DF Miracle de 30 ml – SKU 23658507"
    },
    {
        "imgBack": "nina-perfume.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "8,900 RPGo",
        "titlebottom" : "Nina Ricci",
        "subtitlebottom" : "Nina EDT 30ml Repack 2016– SKU 22656238"
    },
    {
        "imgBack": "davidoff-perfume-searose.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "8,900 RPGo",
        "titlebottom" : "Davidoff",
        "subtitlebottom" : "DF Sea Rose EDT 30 ml – SKU 23960685"
    },
    // {
    //     "imgBack": "ralph-vapo.png",
    //     "pricecurrent": "2,500",
    //     "priceother":"+ S/ 25",
    //     "priceante": "7,900 RPGo",
    //     "titlebottom" : "Ralph Lauren",
    //     "subtitlebottom" : "Ralph EDT Vapo 30 ml – SKU 21631474"
    // },
    {
        "imgBack": "tous-edt.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "7,900 RPGo",
        "titlebottom" : "Tous",
        "subtitlebottom" : "EDT Tous 30 ml 2013 – SKU 15605494"
    },
    // {
    //     "imgBack": "mac-mate-hit.png",
    //     "pricecurrent": "3,500",
    //     "priceother":"+ S/ 20",
    //     "priceante": "7,000 RPGo",
    //     "titlebottom" : "MAC",
    //     "subtitlebottom" : "Labial mate HIT – SKU 17226344"
    // },
    {
        "imgBack": "elizabeth-arden-perfume-red-door.png",
        "pricecurrent": "4,000",
        "priceother":"+ S/ 25",
        "priceante": "7,900 RPGo",
        "titlebottom" : "Elizabeth Arden",
        "subtitlebottom" : "Frag New Red Door Edt 30 ml– SKU 20807273"
    },
    {
        "imgBack": "mac-labial-instigador.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Instigador – SKU 17226347"
    },
    {
        "imgBack": "mac-labial-sube.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Sube  – SKU 17226355"
    },
    {
        "imgBack": "mac-labial-natural.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Natural – SKU 17226352"
    },
    {
        "imgBack": "mac-labial-vibra.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Vibra  – SKU 17226356"
    },
    {
        "imgBack": "Labial-Matte-All-Fired-Up.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate All Fired Up – SKU 15676486"
    },
    {
        "imgBack": "mac-labial-flat-out.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Flat Out Fabulous – SKU 15676487"
    },
    {
        "imgBack": "mac-labial-peligro.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Peligro – SKU 17226343"
    },
    {
        "imgBack": "mac-labial-relentlssly.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Relentlssly – SKU 15676484"
    },
    {
        "imgBack": "mac-labial-sefeliz.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 20",
        "priceante": "7,000 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Labial mate Se Feliz – SKU 17226354"
    },
    {
        "imgBack": "mac-mascara-marron.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 30",
        "priceante": "8,500 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Mascara de pestañas marrón – SKU 16843313"
    },
    {
        "imgBack": "mac-delineador-flingo.png",
        "pricecurrent": "3,000",
        "priceother":"+ S/ 30",
        "priceante": "8,200 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Delineador cejas Flingo – SKU  17780762"
    },
    {
        "imgBack": "mac-mascara-dimension.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 30",
        "priceante": "8,500 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Mascara De Pestañas Dimension– SKU 17431453"
    },
    {
        "imgBack": "mac-delineador-sucio.png",
        "pricecurrent": "3,000",
        "priceother":"+ S/ 30",
        "priceante": "8,200 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Delineador cejas Dirty Blonde– SKU  17780769"
    },
    {
        "imgBack": "DELINEADOR-CEJAS-TAUPE.png",
        "pricecurrent": "3,000",
        "priceother":"+ S/ 30",
        "priceante": "8,200 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Delineador cejas Taupe – SKU  17780760"
    },
    {
        "imgBack": "mac-mascara-haute-naughty.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 30",
        "priceante": "8,500 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Mascara de pestañas Haute Agua – SKU 16715716"
    },
    {
        "imgBack": "mac-mascara-haute-naughty.png",
        "pricecurrent": "3,500",
        "priceother":"+ S/ 30",
        "priceante": "8,500 RPGo",
        "titlebottom" : "MAC",
        "subtitlebottom" : "Mascara pestañas MAC Haute & Naughty Masc black – SKU 13046804"
    }
   ];


  function Paginator(items, page, per_page) {
 
    var page = page || 1,
    per_page = per_page || 10,
    offset = (page - 1) * per_page,
   
    paginatedItems = items.slice(offset).slice(0, per_page),
    total_pages = Math.ceil(items.length / per_page);
    return {
    page: page,
    per_page: per_page,
    pre_page: page - 1 ? page - 1 : null,
    next_page: (total_pages > page) ? page + 1 : null,
    total: items.length,
    total_pages: total_pages,
    data: paginatedItems
    };
  }



