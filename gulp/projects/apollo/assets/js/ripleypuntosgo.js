$(document).ready(function() {
  $('.tool-tip-info').tooltip()
  ClearBoxInit();
  cargarSeccionSegmentacion();
});
var typeDoc = "01";
function cargarSeccionSegmentacion() {
  let pathname = window.location.pathname;
  if (pathname.includes("segmentacion.html")) {
    var objData = localStorage.getItem("objData");
    if (objData != "") {
      let listData = JSON.parse(objData);
      if (listData.length > 0) {
        let clasePuntos = "";
        let clientePuntos = "";
        let nombres = "";
        let clientePuntosObjetivo = "";
        let fechaRipleyPuntos = "";
        let puntaje = "";
        let puntajeFaltante = "";
        let puntajePorcentaje = "";
        let totalRipleyPuntos = "";

        for (let y = 0; y < listData.length; y++) {
          if (listData[y].name == "CLASE_PUNTOS") {
            clasePuntos = listData[y].value;
          }
          if (listData[y].name == "CLASE_PUNTOS_CLIENTES") {
            clientePuntos = listData[y].value;
          }
          if (listData[y].name == "CLASE_PUNTOS_CLIENTES_OBJETIVO") {
            clientePuntosObjetivo = listData[y].value;
          }
          if (listData[y].name == "FECHA_RIPLEYPUNTOS") {
            fechaRipleyPuntos = listData[y].value;
          }
          if (listData[y].name == "NOMBRE") {
            nombres = listData[y].value;
          }
          if (listData[y].name == "PUNTAJE") {
            puntaje = listData[y].value;
          }
          if (listData[y].name == "PUNTAJE_FALTANTE") {
            puntajeFaltante = listData[y].value;
          }
          if (listData[y].name == "PUNTAJE_PORCENTAJE") {
            puntajePorcentaje = listData[y].value;
          }
          if (listData[y].name == "RIPLEYPUNTOS") {
            totalRipleyPuntos = listData[y].value;
          }
        }
        nombres = nombres + "!";
        var windowsize = $(window).width();
        // debugger;
        if (clientePuntos.toUpperCase() == "SILVER PRIME") {
          localStorage.setItem("idSeccionSegmentador", "divSegmentadorSiver");
          $("#divSegmentadorRipley").hide();
          $("#divSegmentadorGold").hide();
          $("#divSegmentadorNoClientes").hide();
          $("#divSegmentadorSiver").show();

          $("#divSegmentadorSiver #nombre").text(nombres);
          $("#divSegmentadorSiver #puntaje").text(puntaje);
          $("#divSegmentadorSiver #puntajeTotal").text(totalRipleyPuntos);
          $("#divSegmentadorSiver #puntajeFaltante").text(puntajeFaltante);
          $("#divSegmentadorSiver #porcentajeBarra").width(
            puntajePorcentaje + "%"
          );
          $("#divSegmentadorSiver #porcentaje").width(puntajePorcentaje + "%");
          $("#divSegmentadorSiver #porcentaje").text(puntajePorcentaje + "%");

          if (windowsize <= 767.98) {
            mostrarSilver();
            // $("#divSegmentadorSiver #seccionSilver").show();
            // $("#divSegmentadorSiver #seccionGold").hide();
            // $("#divSegmentadorSiver #markerGrey").addClass("hr-rpg-grey");
            // $("#divSegmentadorSiver #markerGold").removeClass("hr-rpg-gold");
          }
        } else if (clientePuntos.toUpperCase() == "GOLD PRIME") {
          localStorage.setItem("idSeccionSegmentador", "divSegmentadorGold");
          $("#divSegmentadorRipley").hide();
          $("#divSegmentadorSiver").hide();
          $("#divSegmentadorNoClientes").hide();
          $("#divSegmentadorGold").show();

          $("#divSegmentadorGold #nombre").text(nombres);
          $("#divSegmentadorGold #puntaje").text(puntaje);
          $("#divSegmentadorGold #puntajeTotal").text(totalRipleyPuntos);
          $("#divSegmentadorGold #puntajeFaltante").text(puntajeFaltante);
          $("#divSegmentadorGold #porcentajeBarra").width(
            puntajePorcentaje + "%"
          );
          $("#divSegmentadorGold #porcentaje").width(puntajePorcentaje + "%");
          $("#divSegmentadorGold #porcentaje").text(puntajePorcentaje + "%");

          if (windowsize <= 767.98) {
            mostrarGold();
            // $("#divSegmentadorGold #seccionGold").show();
            // $("#divSegmentadorGold #seccionSilver").hide();
            // $("#divSegmentadorGold #markerGrey").removeClass("hr-rpg-grey");
            // $("#divSegmentadorGold #markerGold").addClass("hr-rpg-gold");
          }

          if (totalRipleyPuntos=="" || totalRipleyPuntos=="0" || totalRipleyPuntos==null || totalRipleyPuntos==undefined ) {
            $(".divNoNormalInfoGold").attr("style", "display:block");
            $(".divNormalInfoGold").attr("style", "display:none");
          }


        } else if (clientePuntos.toUpperCase() == "RIPLEY") {
          localStorage.setItem("idSeccionSegmentador", "divSegmentadorRipley");
          $("#divSegmentadorSiver").hide();
          $("#divSegmentadorGold").hide();
          $("#divSegmentadorNoClientes").hide();
          $("#divSegmentadorRipley").show();

          $("#divSegmentadorRipley #nombre").text(nombres);
          $("#divSegmentadorRipley #puntaje").text(puntaje);
          $("#divSegmentadorRipley #puntajeTotal").text(totalRipleyPuntos);
          $("#divSegmentadorRipley #puntajeFaltante").text(puntajeFaltante);
          $("#divSegmentadorRipley #porcentajeBarra").width(
            puntajePorcentaje + "%"
          );
          $("#divSegmentadorRipley #porcentaje").width(puntajePorcentaje + "%");
          $("#divSegmentadorRipley #porcentaje").text(puntajePorcentaje + "%");

          if (windowsize <= 767.98) {
            mostrarSilver();
            // $("#divSegmentadorRipley #seccionSilver").show();
            // $("#divSegmentadorRipley #seccionGold").hide();
            // $("#divSegmentadorRipley #markerGrey").addClass("hr-rpg-grey");
            // $("#divSegmentadorRipley #markerGold").removeClass("hr-rpg-gold");
          }
        }
      } else {
        $("#divSegmentadorSiver").hide();
        $("#divSegmentadorGold").hide();
        $("#divSegmentadorRipley").hide();
        $("#divSegmentadorNoClientes").show();
      }
    } else {
      $("#divSegmentadorSiver").hide();
      $("#divSegmentadorGold").hide();
      $("#divSegmentadorRipley").hide();
      $("#divSegmentadorNoClientes").show();
    }
  }
}

function RegistroOMPAfiliate() {
  window.location.href = "registro-omp.html";
}

function SolictarTarjeta() {
  window.open(
    "https://www.bancoripley.com.pe/solicita-tarjeta-ripley-mastercard/index.html",
    "_blank"
  );
}

function consultarPointsClient(token) {
  // debugger;
  let document = $("#secctionContenedorClientPrime #nroDocumento").val();
  let client = {
    typeDocument: typeDoc,
    numberDocument: document,
    flagClient: "1",
    captcha: token
  };
  
  localStorage.setItem("objData", "");
  pantallaDeCarga(true);
  getPointsClient(client)
    .done(function(data) {
      if (data.result != "") {
        if (data.result[0] != null) {
          if (data.result.length > 0) {
            let list = data.result[0].data;
            for (let i = 0; i < list.length; i++) {
              if (list[i].customFields.length > 0) {
                let listData = list[i].customFields;
                localStorage.setItem("objData", JSON.stringify(listData));
                ripleyPuntosAnalytics.addEvent(
                  "Segmentacion",
                  "Consulta",
                  "Consulta Exitosa",
                  0
                );
                window.location.href = "./segmentacion.html";
              }
            }
          } else {
            ripleyPuntosAnalytics.addEvent(
              "Segmentacion",
              "Consulta",
              "Consulta No Exitosa",
              0
            );
            window.location.href = "./segmentacion.html";
          }
        } else {
          ripleyPuntosAnalytics.addEvent(
            "Segmentacion",
            "Consulta",
            "Consulta No Exitosa",
            0
          );
          window.location.href = "./segmentacion.html";
        }
      } else {
        ripleyPuntosAnalytics.addEvent(
          "Segmentacion",
          "Consulta",
          "Consulta No Exitosa",
          0
        );
        window.location.href = "./segmentacion.html";
      }
      pantallaDeCarga(false);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      //error
      pantallaDeCarga(false);
    });
}

function consultarPointsClientCP(token) {
  let pathname = window.location.pathname;
  let document = $("#secctionContenedorClientPrimeCP #nroDocumentoCP").val();
  let document2 = $("#secctionContenedorClientPrimeCP #nroDocumentoCP2").val();
  if (document == "") {
    document = document2;
  }
  let client = {
    typeDocument: "01",
    numberDocument: document,
    flagClient: "1",
    captcha: token
  };
  localStorage.setItem("objDataCP", "");
  localStorage.setItem("flg_dniconsulted", "");
  pantallaDeCarga(true);
  getPointsClient(client)
    .done(function(data) {
      if (data.result != "") {
        if (data.result[0] != null) {
          if (data.result.length > 0) {
            let list = data.result[0].data;
            for (let i = 0; i < list.length; i++) {
              if (list[i].customFields.length > 0) {
                let listData = list[i].customFields;
                localStorage.setItem("objDataCP", JSON.stringify(listData));
                ripleyPuntosAnalytics.addEvent(
                  "Segmentacion",
                  "Consulta",
                  "Consulta Exitosa",
                  0
                );
                window.location.href = pathname;
              }
            }
          } else {
            ripleyPuntosAnalytics.addEvent(
              "Segmentacion",
              "Consulta",
              "Consulta No Exitosa",
              0
            );
            window.location.href = pathname;
          }
        } else {
          ripleyPuntosAnalytics.addEvent(
            "Segmentacion",
            "Consulta",
            "Consulta No Exitosa",
            0
          );
          window.location.href = pathname;
        }
      } else {
        ripleyPuntosAnalytics.addEvent(
          "Segmentacion",
          "Consulta",
          "Consulta No Exitosa",
          0
        );
        window.location.href = pathname;
      }
      localStorage.setItem("flg_dniconsulted", "1");
      pantallaDeCarga(false);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      pantallaDeCarga(false);
    });
}

function GetReCaptchaID(containerID) {
  var retval = -1;
  $(".g-recaptcha").each(function(index) {
    if (this.id == containerID) {
      retval = index;
      return;
    }
  });

  return retval;
}

$("#secctionContenedorClientPrime #btnEnviar").on("click", function() {
  // let nroDocumentoValidate = divConsultarPrestamo();
  let nroDocumentoValidate = divNroDocumentoValidate();

  if (!nroDocumentoValidate) {
    //marcacionErrorNroDocumento();
  }
  if (nroDocumentoValidate) {
    // event.preventDefault();
    // var reCaptchaID = GetReCaptchaID("recaptcha");
    // grecaptcha.reset(reCaptchaID);
    // grecaptcha.execute(reCaptchaID);
    grecaptcha.execute();
  }
});

$("#secctionContenedorClientPrimeCP #btnEnviarCP").on("click", function() {
  let nroDocumentoValidate = divConsultarPrestamoCP();

  if (!nroDocumentoValidate) {
    //marcacionErrorNroDocumento();
  }
  if (nroDocumentoValidate) {
    grecaptcha.execute();
  }
});
$("#secctionContenedorClientPrimeCP #btnEnviarCP2").on("click", function() {
  let nroDocumentoValidate = divConsultarPrestamoCP2();

  if (!nroDocumentoValidate) {
    //marcacionErrorNroDocumento();
  }
  if (nroDocumentoValidate) {
    grecaptcha.execute();
  }
});

// $("#secctionContenedorClientPrime #nroDocumento").blur(function() {
//   divConsultarPrestamo();
// });
$("#secctionContenedorClientPrime #nroDocumento").keyup(function () {
  let documentoMessage = $("#secctionContenedorClientPrime #documento-message");
  if (documentoMessage[0].innerText != "")
      divNroDocumentoValidate();
});

function divNroDocumentoValidate() {
  ultimoValorNumeros = "";
  let isValid = true;
  let msgDocumento = "";
  let txtNroDocumento = $("#secctionContenedorClientPrime #nroDocumento");
  let documentoMessage = $("#secctionContenedorClientPrime #documento-message");

  if (!(txtNroDocumento && txtNroDocumento.val() != "")) {
      txtNroDocumento.addClass("is-invalid");
      msgDocumento = "Ingresa tu número de documento.";
      isValid = false;
  }
  else {
      if ($(".boxDni").hasClass("boxSelected")) {
          if (txtNroDocumento.val().length != 8) {
              txtNroDocumento.addClass("is-invalid");
              msgDocumento = "Ingresa tu número de documento.";
              isValid = false;
          }
          else
              txtNroDocumento.removeClass("is-invalid");
      }
      else {
          if (txtNroDocumento.val().length < 9 || txtNroDocumento.val().length > 11) {
              txtNroDocumento.addClass("is-invalid");
              msgDocumento = "Ingresa tu número de documento.";
              isValid = false;
          }
          else
              txtNroDocumento.removeClass("is-invalid");
      }

  }
  if (msgDocumento) {
      documentoMessage.empty();
      documentoMessage.html(msgDocumento);
  }
  return isValid;
}
function divConsultarPrestamo() {
  let isValid = true;
  let msgDocumento = "";
  let txtNroDocumento = $("#secctionContenedorClientPrime #nroDocumento");
  let documentoMessage = $("#secctionContenedorClientPrime #documento-message");

  if (!(txtNroDocumento && txtNroDocumento.val() != "")) {
    txtNroDocumento.addClass("is-invalid");
    msgDocumento = "Ingresa un DNI válido.";
    isValid = false;
  } else {
    if (txtNroDocumento.val().length != 8) {
      txtNroDocumento.addClass("is-invalid");
      msgDocumento = "Ingresa un DNI válido.";
      isValid = false;
    } else txtNroDocumento.removeClass("is-invalid");
  }

  if (msgDocumento) {
    documentoMessage.empty();
    documentoMessage.html(msgDocumento);
  }

  return isValid;
}

$("#secctionContenedorClientPrimeCP #nroDocumentoCP").blur(function() {
  divConsultarPrestamoCP();
});
function divConsultarPrestamoCP() {
  let isValid = true;
  let msgDocumento = "";
  let txtNroDocumento = $("#secctionContenedorClientPrimeCP #nroDocumentoCP");
  let documentoMessage = $(
    "#secctionContenedorClientPrimeCP #documento-messageCP"
  );

  if (!(txtNroDocumento && txtNroDocumento.val() != "")) {
    txtNroDocumento.addClass("is-invalid");
    msgDocumento = "Ingresa un DNI válido.";
    isValid = false;
  } else {
    if (txtNroDocumento.val().length != 8) {
      txtNroDocumento.addClass("is-invalid");
      msgDocumento = "Ingresa un DNI válido.";
      isValid = false;
    } else txtNroDocumento.removeClass("is-invalid");
  }

  if (msgDocumento) {
    documentoMessage.empty();
    documentoMessage.html(msgDocumento);
  }

  return isValid;
}
$("#secctionContenedorClientPrimeCP #nroDocumentoCP2").blur(function() {
  divConsultarPrestamoCP2();
});
function divConsultarPrestamoCP2() {
  let isValid = true;
  let msgDocumento = "";
  let txtNroDocumento = $("#secctionContenedorClientPrimeCP #nroDocumentoCP2");
  let documentoMessage = $(
    "#secctionContenedorClientPrimeCP #documento-messageCP2"
  );

  if (!(txtNroDocumento && txtNroDocumento.val() != "")) {
    txtNroDocumento.addClass("is-invalid");
    msgDocumento = "Ingresa un DNI válido.";
    isValid = false;
  } else {
    if (txtNroDocumento.val().length != 8) {
      txtNroDocumento.addClass("is-invalid");
      msgDocumento = "Ingresa un DNI válido.";
      isValid = false;
    } else txtNroDocumento.removeClass("is-invalid");
  }

  if (msgDocumento) {
    documentoMessage.empty();
    documentoMessage.html(msgDocumento);
  }

  return isValid;
}

function mostrarSilver() {
  let idSecSegmentador = localStorage.getItem("idSeccionSegmentador");
  $("#" + idSecSegmentador + " #markerGold").hide();
  $("#" + idSecSegmentador + " #markerGrey").show();

  $("#" + idSecSegmentador + " #seccionGold").animate(
    {
      "margin-left": "100%"
    },
    500
  );

  $("#" + idSecSegmentador + " #seccionSilver").animate(
    {
      "margin-left": "0%"
    },
    500
  );
  setTimeout(function(){  $("#" + idSecSegmentador + " #seccionGold").css("display", "none"); }, 400);
 
}
function mostrarGold() {
  let idSecSegmentador = localStorage.getItem("idSeccionSegmentador");
  $("#" + idSecSegmentador + " #markerGrey").hide();
  $("#" + idSecSegmentador + " #markerGold").show();
  $("#" + idSecSegmentador + " #seccionGold").css("display", "block");
  $("#" + idSecSegmentador + " #seccionSilver").animate(
    {
      "margin-left": "-100%"
    },
    500
  );
  $("#" + idSecSegmentador + " #seccionGold").animate(
    {
      "margin-left": "0%"
    },
    500
  );
}
function pantallaDeCarga(booleano) {
  if (booleano) {
    $(".app-opacity").show();
  } else {
    $(".app-opacity").hide();
  }
}
$("#appModalPuntosCalificables").on("show.bs.modal", function(e) {
  $("#header").addClass("opacity_ModalPC");
});
$("#appModalPuntosCalificables").on("hide.bs.modal", function(e) {
  $("#header").removeClass("opacity_ModalPC");
});
function selectedTypeDoc(type) {
    typeDoc = type;
}
$(".boxDni").on('click', function () {
  ClearBoxInit();
  $(".boxDni").addClass("boxSelected");
  $(".boxCe").removeClass("boxSelected");
  $(".textDni").addClass("textSelected");
  $(".textDni").removeClass("textUnselected");
  $(".textCe").removeClass("textSelected");
  $(".textCe").addClass("textUnselected");
  $("#nroDocumento").attr('maxlength', '8');
});

$(".boxCe").on('click', function () {
  ClearBoxInit();
  $(".boxCe").addClass("boxSelected");
  $(".boxDni").removeClass("boxSelected");
  $(".textCe").addClass("textSelected");
  $(".textCe").removeClass("textUnselected");
  $(".textDni").addClass("textUnselected");
  $(".textDni").removeClass("textSelected");
  $("#nroDocumento").attr('maxlength', '11');
});
function ClearBoxInit() {
  $("#secctionContenedorClientPrime #nroDocumento").val("");
  $("#secctionContenedorClientPrime #nroDocumento").removeClass("is-invalid");
  $("#secctionContenedorClientPrime #documento-message").empty();
}