var gulp = require('gulp');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var browserSync =  require('browser-sync').create();
var rename = require('gulp-rename');
var plumber =  require('gulp-plumber');
var autoprefixer =  require('gulp-autoprefixer');
const image = require('gulp-image');
var minify = require('gulp-minify');
var uglifycss = require('gulp-uglifycss');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var postcss = require('gulp-postcss');
var atImport = require('postcss-import');
//var mqpacker = require('css-mqpacker');
// var cssnano = require('cssnano');
//var cssnano = require('gulp-cssnano');


// var javascriptObfuscator = require('gulp-javascript-obfuscator');

var rutaApollo = './projects/apollo',
    rutaChallenger = './projects/challenger',
    rutaColumbia = './projects/columbia';

gulp.task('ripleypuntos',function () {
    return gulp.src(rutaApollo + '/assets/pug/ripleyPuntosGo.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('ripley-puntos.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('segmentador',function () {
    return gulp.src(rutaApollo + '/assets/pug/segmentador.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('segmentacion.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('registro-omp',function () {
    return gulp.src(rutaApollo + '/assets/pug/registro-omp.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('registro-omp.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});

gulp.task('categorias',function () {
    return gulp.src(rutaApollo + '/assets/pug/categorias.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('clientes-prime.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('canjes-promociones',function () {
    return gulp.src(rutaApollo + '/assets/pug/canjes-promociones.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('canjes.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('aunNoTienesTarjeta',function () {
    return gulp.src(rutaApollo + '/assets/pug/aunNoTienesTarjeta.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('afiliacion.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('promociones',function () {
    return gulp.src(rutaApollo + '/assets/pug/promociones.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('promociones.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});

// Otros
gulp.task('error404',function () {
    return gulp.src(rutaApollo + '/assets/pug/error404.pug')
        .pipe(pug({
            doctype:'html'
        }))
        .pipe(rename('error404.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});

//-----------------------------------------------------------
gulp.task('pugHBR', [
    'ripleypuntos',
    'categorias',
    'canjes-promociones',
    'promociones',
    'segmentador',
    'registro-omp',
    'aunNoTienesTarjeta',
    'error404'
],function () {
    return gulp.src(rutaApollo + '/assets/pug/homeHBR.pug')
        .pipe(pug({
            doctype:'html'
            //pretty:true
        }))
        .pipe(rename('index.html'))
        .pipe(plumber())
        .pipe(gulp.dest(rutaApollo + '/dist'))
});

gulp.task('sassHBR',[/*'corta-claraHBR',*/'bootstrapHBR'],function () {
    return gulp.src([
            rutaApollo + '/assets/scss/bancoRipleycom.scss',
            rutaApollo + '/assets/scss/ripley-puntos-go.scss'
        ])
        .pipe(sass().on('error', sass.logError))
        .pipe(plumber())
        .pipe(uglifycss())
        .pipe(concat('styleHB.css'))
        .pipe(gulp.dest(rutaApollo + '/dist/css'))
});
gulp.task('bootstrapHBR',function () {
    return gulp.src(rutaApollo + '/assets/scss/bootstrap.min.css')
        .pipe(gulp.dest(rutaApollo + '/dist/css'))
});
gulp.task('pdfHBR',function () {
    return gulp.src(rutaApollo + '/assets/pdf/*.pdf')
        .pipe(gulp.dest(rutaApollo + '/dist/pdf'))
});
gulp.task('excelHBR',function () {
    return gulp.src([rutaApollo + '/assets/excel/*.xls', rutaApollo + '/assets/excel/*.xlsx', rutaApollo + '/assets/excel/*.xlsm'])
        .pipe(gulp.dest(rutaApollo + '/dist/excel'))
});
gulp.task('seoHBR',function () {
    return gulp.src(rutaApollo + '/assets/seo/*')
        .pipe(gulp.dest(rutaApollo + '/dist'))
});
gulp.task('jsHBR', function() {
    return gulp.src([
        rutaApollo + '/assets/js/jquery.min.js',
        rutaApollo + '/assets/js/popper.js',
        rutaApollo + '/assets/js/bootstrap.min.js',
        
        rutaApollo + '/assets/js/jquery-lazy.min.js',
        rutaApollo + '/assets/js/lory.min.js',
        rutaApollo + '/assets/js/homebanking.js',
        rutaApollo + '/assets/js/services.js',
        rutaApollo + '/assets/js/ripleypuntosgo.js',
        rutaApollo + '/assets/js/clienteomp.js',
        rutaApollo + '/assets/js/promotion.js',
        rutaApollo + '/assets/js/categorias-beneficios.js',
        rutaApollo + '/assets/js/items-canjes-promociones.js',
        rutaApollo + '/assets/js/canjes-promociones.js',
        rutaApollo + '/assets/js/js-homeHBR.js'])
        .pipe(concat('jsHB.js'))
        .pipe(minify())
        //.pipe(javascriptObfuscator())
        .pipe(gulp.dest(rutaApollo + '/dist/js'));
});
gulp.task('gtag', function() {
    return gulp.src([
        rutaApollo + '/assets/js/gtag.js'])
        .pipe(concat('gtag.js'))
        .pipe(minify())
        //.pipe(javascriptObfuscator())
        .pipe(gulp.dest(rutaApollo + '/dist/js'));
});
gulp.task('bsHBR',['pugHBR','sassHBR','jsHBR', 'gtag'],function () {
    browserSync.init({
        server:{
            baseDir:rutaApollo + "/dist/"
        },
        watchOptions:{
            awaitWriteFinish : true
        }
    });
    gulp.watch(rutaApollo + '/assets/scss/**/*.scss',['sassHBR']);
    gulp.watch(rutaApollo + '/assets/pug/**/*.pug',['pugHBR']);
    gulp.watch(rutaApollo + '/assets/js/*.js',['jsHBR']);
    //gulp.watch(rutaApollo + '/assets/scss/**/*.scss').on('change',browserSync.reload);
    //gulp.watch(rutaApollo + '/assets/pug/**/*.pug').on('change',browserSync.reload);
    gulp.watch(rutaApollo + '/dist/css/*.css').on('change',browserSync.reload);
    gulp.watch(rutaApollo + '/dist/js/*.js').on('change',browserSync.reload);
    gulp.watch(rutaApollo + '/dist/*.html').on('change',browserSync.reload);
});
gulp.task('compHBR',['pugHBR','sassHBR','jsHBR','bsHBR','pdfHBR', 'seoHBR', 'excelHBR', 'gtag'],function () {
});
gulp.task('imageHBR',function () {
   return gulp.src(rutaApollo + '/assets/img/**/*')
       .pipe(image())
       .pipe(gulp.dest(rutaApollo + '/dist/img'))
});

gulp.task('imgFooter',function () {
    return gulp.src(rutaApollo + '/assets/img/footer-img/*')
        .pipe(image())
        .pipe(gulp.dest(rutaApollo + '/dist/img/footer-img'))
});
gulp.task('imgHome',function () {
    return gulp.src(rutaApollo + '/assets/img/home/*')
        .pipe(image())
        .pipe(gulp.dest(rutaApollo + '/dist/img/home'))
});

gulp.task('imgRPGO',function () {
    return gulp.src(rutaApollo + '/assets/img/RPGO/*')
        .pipe(image())
        .pipe(gulp.dest(rutaApollo + '/dist/img/RPGO'))
});
gulp.task('imgSVG',function () {
    return gulp.src(rutaApollo + '/assets/img/svg/*')
        .pipe(image())
        .pipe(gulp.dest(rutaApollo + '/dist/img/svg'))
});

gulp.task('HBR',['pugHBR','sassHBR','jsHBR','imageHBR', 'pdfHBR', 'seoHBR', 'excelHBR', 'gtag'],function () {
});

